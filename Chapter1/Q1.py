'''Implement an algorithm to determine if a string has all unique characters.
 What if you cannot use additional data structures?'''


def is_unique(string):
    def is_unique_without_extra_data_structure(string):
        #string = string.lower()
        checker = 0
        for i in string:
            num  = ord(i) - ord('A')

            if (checker & 1 << num) > 0:
                return False
            else:
                checker = checker | 1 << num

        return True

    return is_unique_without_extra_data_structure(string)

print(is_unique('Abhis'))